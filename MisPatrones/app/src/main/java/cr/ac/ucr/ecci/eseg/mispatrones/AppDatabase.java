package cr.ac.ucr.ecci.eseg.mispatrones;
import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Persona.class}, version = 1, exportSchema = false)
//Clase para la conexion de la base de datos
public abstract class AppDatabase extends RoomDatabase {
    public abstract PersonaDAO personaDAO();
}
