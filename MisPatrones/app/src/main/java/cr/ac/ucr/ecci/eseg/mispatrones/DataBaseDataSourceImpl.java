package cr.ac.ucr.ecci.eseg.mispatrones;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class DataBaseDataSourceImpl extends AppCompatActivity implements DataBaseDataSource {

    @Override
    public List<Object> obtainItems() throws BaseDataItemsException {
        List<Object> items;
        try {
// TODO: Obtener de la base de datos
            items = createArrayList();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }
    private List<Object> createArrayList(){
        List<Object> newArrayList = DatabaseGenerator.getBasePrecargada();
        return newArrayList;
    }
}
