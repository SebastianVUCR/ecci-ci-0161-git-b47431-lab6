package cr.ac.ucr.ecci.eseg.mispatrones;

import java.util.List;
public interface DataBaseDataSource {
    List<Object> obtainItems () throws BaseDataItemsException;
}