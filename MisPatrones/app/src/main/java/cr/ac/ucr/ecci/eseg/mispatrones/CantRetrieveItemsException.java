package cr.ac.ucr.ecci.eseg.mispatrones;

public class CantRetrieveItemsException extends Exception {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
