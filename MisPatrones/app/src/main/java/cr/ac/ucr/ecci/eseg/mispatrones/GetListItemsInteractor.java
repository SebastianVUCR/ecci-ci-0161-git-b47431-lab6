package cr.ac.ucr.ecci.eseg.mispatrones;

import java.util.List;
public interface GetListItemsInteractor {
    interface OnFinishedListener {
        void onFinished(List<Object> items);
    }
    void getItems(OnFinishedListener listener);
}