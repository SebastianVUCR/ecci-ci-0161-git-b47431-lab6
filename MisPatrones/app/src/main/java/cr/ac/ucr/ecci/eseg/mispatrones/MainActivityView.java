package cr.ac.ucr.ecci.eseg.mispatrones;

import java.util.List;
public interface MainActivityView {
    // Mostrar el progreso en la UI del avance de la tarea a realizar
    void showProgress();
    // Esconder el indicador de progreso de la UI
    void hideProgress();
    // Mostrar los items de la lista en la UI
    void setItems(List<Object> items);
    // Mostrar mensaje en la UI
    void showMessage(Object message);
}
