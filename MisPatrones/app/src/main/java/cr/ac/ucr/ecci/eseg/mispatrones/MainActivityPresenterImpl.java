package cr.ac.ucr.ecci.eseg.mispatrones;

import java.util.List;
public class MainActivityPresenterImpl implements
        MainActivityPresenter,
        GetListItemsInteractor.OnFinishedListener{

    private MainActivityView mMainActivityView;
    private GetListItemsInteractor mGetListItemsInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView){
        this.mMainActivityView=mainActivityView;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor=new GetListItemsInteractorImpl();
    }

    @Override public void onResume(){
        if(mMainActivityView!=null){
            mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        mGetListItemsInteractor.getItems(this);
    }
    // Evento de clic en la lista
    @Override public void onItemClicked(int position){
        if(mMainActivityView!=null){
           mMainActivityView.showMessage(String.format("Position %d clicked",position+1));
        }
    }
    @Override public void onDestroy(){
        mMainActivityView=null;
    }
    @Override public void onFinished(List<Object> items){
        if(mMainActivityView!=null){
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView(){
        return mMainActivityView;
    }
}
