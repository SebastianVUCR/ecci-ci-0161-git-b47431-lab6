package cr.ac.ucr.ecci.eseg.mispatrones;

import java.util.List;
public class ItemsRepositoryImpl implements ItemsRepository {
    private DataBaseDataSource mDataBaseDataSource;
    @Override
    public List<Object> obtainItems() throws CantRetrieveItemsException {
        List<Object> items = null;
        try {
            mDataBaseDataSource = new DataBaseDataSourceImpl();
            items = mDataBaseDataSource.obtainItems();
        } catch (BaseDataItemsException e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
        return items;
    }
}