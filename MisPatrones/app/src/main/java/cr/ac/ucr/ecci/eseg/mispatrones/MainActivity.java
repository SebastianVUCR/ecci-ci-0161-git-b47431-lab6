package cr.ac.ucr.ecci.eseg.mispatrones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.util.List;
public class MainActivity extends AppCompatActivity implements MainActivityView,
        AdapterView.OnItemClickListener {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;
    DatabaseGenerator db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        db = new DatabaseGenerator(getApplicationContext());
        //llena la base de datos
        try {
            db.generateDatabase();
        }
        catch(Exception e)
        {
            //si se cargaron los datos en una ejecucion anterior
            Toast.makeText(getApplicationContext(), "La base de datos ya estaba cargada", Toast.LENGTH_SHORT).show();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = (ListView) findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        // Llamada al Presenter
        mMainActivityPresenter = new MainActivityPresenterImpl(this);
    }

    @Override protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }

    // Esconder el indicador de progreso de la UI
    @Override public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }

    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override public void setItems(List<Object> items) {
        mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                items));
    }

    // Mostrar mensaje en la UI
    @Override public void showMessage(Object message) {
        Toast.makeText(this, message.toString(), Toast.LENGTH_SHORT).show();
    }

    // Evento al dar clic en la lista
    @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        //Toast.makeText(this, Integer.toString(position), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(mListView.getContext(), DetailsActivity.class);
        intent.putExtra("position", position);
        mListView.getContext().startActivity(intent);
    }
}