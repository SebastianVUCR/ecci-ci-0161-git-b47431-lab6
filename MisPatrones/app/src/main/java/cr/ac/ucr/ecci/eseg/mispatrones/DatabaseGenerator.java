package cr.ac.ucr.ecci.eseg.mispatrones;

import android.content.Context;
import android.os.AsyncTask;

import androidx.room.Room;


import java.util.ArrayList;
import java.util.List;

public class DatabaseGenerator {

    AppDatabase db;

    public DatabaseGenerator(Context context) {
        db = Room.databaseBuilder(context, AppDatabase.class, "andoidStorage").build();
    }

    public void generateDatabase() {
        new insertarPersona().execute(
                new Persona("1","Sebas","NA"),
                new Persona("2","Juan","NA"),
                new Persona("3","Alexa","NA"),
                new Persona("4","Sergio","NA"),
                new Persona("5","Melina","NA"),
                new Persona("6","Daniela","NA"),
                new Persona("7","Kevin","NA")
        );

    }

    public static List<Object> getBasePrecargada()
    {
        List<Object> newArrayList = new ArrayList<>();
        newArrayList.add(new Persona("1","Sebas","NA"));
        newArrayList.add(new Persona("2","Juan","NA"));
        newArrayList.add(new Persona("3","Alexa","NA"));
        newArrayList.add(new Persona("4","Sergio","NA"));
        newArrayList.add(new Persona("5","Melina","NA"));
        newArrayList.add(new Persona("6","Daniela","NA"));
        newArrayList.add(new Persona("7","Kevin","NA"));
        return newArrayList;
    }

    public void selectPersonas() {
        new asyncSelectPersonas().execute();
    }

    public void findPersonas(Object id) {
        new asyncFindPersona().execute(id.toString());
    }

    public class asyncFindPersona extends AsyncTask<String, Object, Persona>{
        Persona persona;

        @Override
        protected Persona doInBackground(String ...strings) {

            return db.personaDAO().findPersona(strings[0]);
        }

        @Override
        protected void onPostExecute (Persona result) {
            DetailsActivity.actualizarInterfaz(result);
        }

    }

    public class asyncSelectPersonas extends AsyncTask<Persona, List<Object>, List<Persona>> {
        Persona Persona;

        @Override
        protected List<Persona> doInBackground(Persona... Personas) {

            return db.personaDAO().leerPersonas();
        }

        @Override
        protected void onPostExecute (List<Persona> result) {
            // DetailsActivity.actualizarInterfaz(result);
        }

    }

    public class insertarPersona extends AsyncTask<Persona, Void, Void> {
        Persona Persona;

        @Override
        protected Void doInBackground(Persona... Personas) {
                Persona = Personas[0];
            try {
                db.personaDAO().insertar(Personas);
            }
            catch(Exception e)
            {
                //la base de datos ya estaba cargada
            }

            return null;
        }
    }

}
