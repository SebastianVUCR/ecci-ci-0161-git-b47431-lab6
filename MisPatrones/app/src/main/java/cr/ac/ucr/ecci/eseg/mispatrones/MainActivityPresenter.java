package cr.ac.ucr.ecci.eseg.mispatrones;

public interface MainActivityPresenter {
    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position);
    // destruir
    void onDestroy();
}