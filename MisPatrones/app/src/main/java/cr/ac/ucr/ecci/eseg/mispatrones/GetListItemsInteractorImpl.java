package cr.ac.ucr.ecci.eseg.mispatrones;

import android.os.Handler;
import java.util.List;
public class GetListItemsInteractorImpl implements GetListItemsInteractor {
    private ItemsRepository mItemsRepository;
    @Override public void getItems(final OnFinishedListener listener) {
// Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                List<Object> items = null;
                mItemsRepository = new ItemsRepositoryImpl();
                try {
// obtenemos los items
                    items = mItemsRepository.obtainItems();
                } catch (CantRetrieveItemsException e) {
                    e.printStackTrace();
                }
// Al finalizar retornamos los items
                listener.onFinished(items);
            }
        }, 0);
    }
}