package cr.ac.ucr.ecci.eseg.mispatrones;

public class BaseDataItemsException extends Exception {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}
