package cr.ac.ucr.ecci.eseg.mispatrones;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity{

    private DetailsActivityPresenter mMainActivityPresenter;
    static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalles_persona);
        Bundle bundle = getIntent().getExtras();
        int position = -1; // or other values
        if (bundle != null)
            position = bundle.getInt("position");
        TextView tv1 = (TextView) findViewById(R.id.id);
        tv1.setText(Integer.toString(position));
        DatabaseGenerator db = new DatabaseGenerator(getApplicationContext());
        db.findPersonas(position + 1);
        //String tmp = db.selectPersonas().toString();
        //Toast.makeText(this, tmp, Toast.LENGTH_SHORT).show();
    }

    public static void actualizarInterfaz(Persona persona) {
        //hay que obtener la fila que se quieren en otro lugar
        Toast.makeText(context, persona.getIdentificacion()+
                persona.getNombre() +
                persona.getIdImagen(), Toast.LENGTH_SHORT).show();
    }

    @Override public void onDestroy() {
        super.onDestroy();
    }

    @Override public void onResume() {
        super.onResume();
    }
}
