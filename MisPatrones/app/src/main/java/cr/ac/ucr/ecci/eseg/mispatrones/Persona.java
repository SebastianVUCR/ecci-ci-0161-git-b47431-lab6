package cr.ac.ucr.ecci.eseg.mispatrones;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Persona implements Parcelable {

    @PrimaryKey @NonNull
    private String identificacion;
    private String nombre;
    private String idImagen;


    public Persona(String identificacion, String nombre, String idImagen) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.idImagen = idImagen;
    }

    protected Persona(Parcel in) {
        if (in.readByte() == 0) {
            this.identificacion = null;
        } else {
            this.identificacion = in.readString();
        }
        if (in.readByte() == 0) {
            this.nombre = null;
        } else {
            this.nombre = in.readString();
        }
        if (in.readByte() == 0) {
            this.idImagen = null;
        } else {
            this.idImagen = in.readString();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        dest.writeString(idImagen);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @Override
    public String toString() {
        return nombre;
    }


    @NonNull
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(@NonNull String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(String idImagen) {
        this.idImagen = idImagen;
    }
}