package cr.ac.ucr.ecci.eseg.mispatrones;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

//para ejecutar consultas en la base de datos
@Dao
public interface PersonaDAO {
    @Query("SELECT * FROM Persona")
    List<Persona> leerPersonas();
    @Query("SELECT * FROM Persona WHERE identificacion = :id")
    Persona findPersona(String id);
    @Insert
    void insertar(Persona... Personas);
    @Delete
    void borrar(Persona Persona);
    @Update
    void actualizar(Persona Persona);
}