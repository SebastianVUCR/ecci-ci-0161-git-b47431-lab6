package cr.ac.ucr.ecci.eseg.mispatrones;

public interface DetailsActivityPresenter {
    // resumir
    void onResume();
    // destruir
    void onDestroy();
    // crear
    void onCreate();
}

