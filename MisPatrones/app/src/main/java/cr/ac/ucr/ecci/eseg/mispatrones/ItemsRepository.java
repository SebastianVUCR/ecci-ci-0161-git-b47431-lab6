package cr.ac.ucr.ecci.eseg.mispatrones;

import java.util.List;
public interface ItemsRepository {
    List<Object> obtainItems() throws
            CantRetrieveItemsException;
}